package br.com.senac.livraria.banco;

import br.com.senac.livraria.entity.Autor;

public class AutorDAO extends DAO<Autor> {

    public AutorDAO() {
        super(Autor.class);
    }

}
